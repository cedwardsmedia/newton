	</div></div></div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://code.jquery.com/jquery.js"></script>
        

        
        <div id="footer">
              <div class="container">
                <p class="text-muted">
                    <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"></a> Unless otherwise noted, all content contained within Codename "Newton" is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a><br />Codename "Newton" and the <i class="fa fa-code"></i> logo are trademarks of Corey Edwards.<br /> &copy; 2014 Corey Edwards - All Rights Reserved.
                </p>
              </div>
        </div>

        
        <!-- Insert Credits Script -->
        <script src="http://www.cedwardsmedia.com/credit.js"></script>
        <!-- End Credits Script -->

</body>

</html>
