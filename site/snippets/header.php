<!DOCTYPE html>
<html lang="en">
<head>
	
	<title><?= html($page->title()) ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="icon" type="image/png" href="<? echo $site->url(); ?>/assets/images/favicon.png" />
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
	
	<!-- Optional theme -->
	
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	<?= css('assets/styles/styles.css') ?>
	<?= css('assets/styles/php.css') ?>
	<?= css('assets/styles/html.css') ?>
	<?= css('assets/styles/js.css') ?>
	<?= css('assets/styles/css.css') ?>
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="wrap">
	<div id="wrapper">
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
		<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#"><i class="fa fa-code"></i> newton</a>
		</div>
		<div class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
		<li class="active"><a href="#">Home</a></li>
		<li><a href="#about">About</a></li>
		<li><a href="#contact">Contact</a></li>
		</li>
		</ul>
		<p class="navbar-text pull-right">
		<a href="http://www.twitter.com/cedwardsmedia" target="_blank" class="navbar-link"><i class="fa fa-twitter fa-lg"></i></a>&nbsp;&nbsp;<a href="http://www.gittip.com/cedwardsmedia" target="_blank" class="navbar-link"><i class="fa fa-gittip fa-lg"></i></a>&nbsp;&nbsp;<a href="https://github.com/cedwardsmedia/newton" target="_blank" class="navbar-link"><i class="fa fa-github-alt fa-lg"></i></a>
		</p>
		</div><!--/.nav-collapse -->
		</div>
	</div>
	
	<div id="content">
		<div class="menu">
			<a id="logo" href="<? echo $site->url(); ?>"></a>
			<form id="search" action="./search">
				<div class="input-group">
					<input type="search" class="form-control input-sm " name="q" placeholder="Search...">
					<div class="input-group-btn">
					<button class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-search"></span></button>
					</div>
				</div>
			</form>
			<?php snippet('treemenu') ?>
		</div>
