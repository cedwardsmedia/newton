<?php 

class kirbytextExtended extends kirbytext {
  
  function __construct($text, $markdown=true) {
    
    parent::__construct($text, $markdown);
    
    // define custom tags
    $this->addTags('lead');
    $this->addAttributes('text');
                    
  }  

function lead($params) {
  
  $text = $params['lead'];
  
  return "<p class=\"lead\">" . html($text) . "</p>";
    
}

}

?>